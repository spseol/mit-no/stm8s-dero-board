# STM8S Dero Board

STM8S Dero (DEv zeRO) Board

Vývojová deska navržená pro studenty [SPŠEOL](https://spseol.cz/), kteří s ní mohou experimentovat nejen při výuce mikroprocesorové techniky.

⚠️ Deska je určena k vložení do [kontaktního pole](https://cs.wikipedia.org/wiki/Nep%C3%A1jiv%C3%A9_pole), pokud konektor `J1` či `J4` dáte ze spodní strany desky (_strana s logem open hardware_) nebudete je moci využívat, protože budou pod deskou. Navíc při vložení desky do pole (_to je ta fatální část problému_)
by došlo ke zkratu napájecího jumperu `J1` s GPIO konektorem `J2`, což způsobí __zkrat napájecího zdroje__, jenž by mohl vést k __poškození__ vašeho __plošného spoje__, případně __napájecího zdroje__ samotného. Dále by došlo ke zkratu programovacího konektoru `J4` a GPIO konektoru `J3`, pokud však bude napájení ve zkratu, nemělo by to [MCU](https://cs.wikipedia.org/wiki/Jedno%C4%8Dipov%C3%BD_po%C4%8D%C3%ADta%C4%8D) ohrozit.

❗Pokud jste __[tutan](https://cestina20.cz/slovnik/tutan/)__, věnujte prosím zvýšenou pozornost __orientaci konektorů__ na následujícím obrázku!

![demo](doc/demo.png)

## Blokové schéma

```mermaid
flowchart LR
    PWR == +VIN ==> LDO == +3V3 ==> JMP
    PWR == +VIN ==> JMP == VDD ==> MCU
    MCU -- SWIM <--> ST-Link -- USB <--> PC
    MCU <--> HEADER_A[Pin Header A]
    MCU <--> HEADER_B[Pin Header B]
    MCU --> LED
    BTN -- RESET --> MCU
```

## Parametry

Shrnutí základních parametrů Dero Board.

### Napájení

Desku je možné napájet ve dvou režimech nastavitelných zkratovací propojkou. Zkratovací propojka propojuje interní napájecí větev `VDD` s větví `3V3` případně `VIN`.

#### Přímé napájení s `VIN`

V tomto režimu je pro korektní činnost zařízení dodržet požadavky na napájení MCU, tedy přivést na `VIN` napětí od `2.95V` do `5.5V`.

#### Napájení stabilizátorem `3V3`

Na desce je integrovaný nízko úbytkový lineární stabilizátor (LDO), který je napájen z větve `VIN`. Tento stabilizátor poskytuje stabilní napětí `3.3V` pro napájení MCU, pokud je na vstup `VIN` přivedeno napětí od `4.5V` do `15V`. Výstup LDO, tedy větev `3V3` je vyvedena i na `Pin Header A` a lze ji tedy využít k napájení vyvíjené aplikace. V tom případě je třeba zjistit, jaká bude výkonová ztráta na LDO a případně zajistit jeho chlazení.

### MCU

Deska je osazena 8b mikrokontrolérem [STM8S103F3M6](https://www.st.com/resource/en/datasheet/stm8s103f2.pdf). Přehled pamětí použitého MCU shrnuje následující tabulka, je možné jej taktovat až na `16MHz`, podrobnější popis MCU můžete najít v [datasheetu](https://www.st.com/resource/en/datasheet/stm8s103f2.pdf).

| Paměť  | Velikost |
| ------ | -------- |
| RAM    | `1 KiB`  |
| Flash  | `8 KiB`  |
| EEPROM | `640 B`  |


### Resetovací tlačítko

Deska má integrované i resetovací tlačítko, které slouží pro restart MCU.

### Osciloskop pro chudé

Na desce je i jedna LED, připojená na `PD4`, kterou je možné použít pro ladění aplikace.

### Programovací konektor

Desku je možné programovat pomocí programovacího konektoru pinově kompatibilního s ST-Link V2. Na konektor je přivedeno napětí aplikace a signály `SWIM` a `NRST`.

## Výkresová dokumentace

Výkresová dokumentace ve formátu `PDF` je ve složce `doc`.

* [Schéma](https://gitlab.com/wykys/stm8s-dero-board/-/blob/main/doc/dero-board.pdf)
* [Osazovací výkres horní vrstva](https://gitlab.com/wykys/stm8s-dero-board/-/blob/main/doc/drawing-top.pdf)
* [Osazovací výkres dolní vrstva](https://gitlab.com/wykys/stm8s-dero-board/-/blob/main/doc/drawing-bot.pdf)
* [Vrtací výkres](https://gitlab.com/wykys/stm8s-dero-board/-/blob/main/doc/drl-map.pdf) - všechny díry jsou prokovené


## Inrataktivní seznam součástek

* [Bill of materials](https://wykys.gitlab.io/stm8s-dero-board/ibom.html) - BOM

## Zdroje

* [Application note STM8S](https://www.st.com/resource/en/application_note/cd00194637-getting-started-with-stm8s-and-stm8af-microcontrollers--stmicroelectronics.pdf)
* [Datashteet STM8S103F3](https://www.st.com/resource/en/datasheet/stm8s103f2.pdf)
* [Reference manual STM8S](https://www.st.com/resource/en/reference_manual/cd00190271-stm8s-series-and-stm8af-series-8bit-microcontrollers-stmicroelectronics.pdf)
* [Programming manual STM8](https://www.st.com/resource/en/programming_manual/pm0044-stm8-cpu-programming-manual-stmicroelectronics.pdf)
